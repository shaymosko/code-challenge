import argparse
import flask
import requests


arg_parser = argparse.ArgumentParser(description="an instance the server side of the todo list")
arg_parser.add_argument("--host", required=True, help="the ip of the public server")
arg_parser.add_argument("--ip", dest="server_ip", required=True, help="the ip of the backend")
arg_parser.add_argument("-p", dest="server_port", required=True, type=int, help="the port of the backend")
args = arg_parser.parse_args()

host_ip = args.host

app = flask.Flask(__name__)
base_url = f"http://{args.server_ip}:{args.server_port}/tasks"


@app.route('/')
def root():
    tasks = requests.get(url=base_url).json()
    return tasks


# @app.route('/tasks/<string:task_id>')
# def show_task(task_id: str):
#     res = requests.get(url=base_url + "/{task_id}").json()
#     return res


if __name__ == '__main__':
    app.run(host=args.host)
