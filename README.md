# code challenge

## current capabilities
back-end is working
upon start-up, the app will try to get the tasks from other servers
when update in the tasks occurs the server sends the data to the other servers.
the more up-to-date timestamp of a task wins

front-end is non-existent, displays the tasks in json for now 
the ones that weren't removed
## potential bugs
### when data update occurs, the data will be sent endlessly between the hosts - FIXED
###

## how to run
needs to run from different folder, creates the db in the cwd 
### for each instance of the backend you'll run
```cmd
python3 db\main.py --name <server regeion>
```

currently supports eu, na
### for each instance of the server you'll run
```cmd
python3 server\server.py --host <server_ip> --ip <backend_ip> -p <backend_port>
```



to add tasks, use the api :)


## initial design
### front end
as far as the front-end would access the information I'd like it would be dumb.
- connects to the back-end, different server to each region
- Receive tasks from the back-end and displays them to the user,
-  ```optional``` log as user
- 'refresh' would be every time the back-end has something new to present,
- or it could be every X seconds.
- Either way conflicts may occur.

### back end

- allow clients to pull data, and edit the data as they wish
- firstly, use json for the DB, consider switch the DB to a more suitable one (MySQL, etc.)
- multiple instances may communicate with each other to synchronize
- use FastAPI like to communicate

# final design
### front
- didn't do anything
### bask-end
- allow clients to pull data, and edit the data as they wish
- sqlite db for the data
- multiple instances may communicate with each other to synchronize
- uses FastAPI to communicate
- 
 
