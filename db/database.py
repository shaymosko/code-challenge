import sqlite3
from typing import List, Optional

import requests

from \
    tasks import Task, TaskNotFoundException, task2dict, TaskNotDoneException
from utils import get_known_servers_bindings, Peer
from consts import INTERNAL_REQUEST
import multiprocessing
import datetime
import consts


class Database:
    def __init__(self, db="db.db"):
        self.peers = get_known_servers_bindings()
        self.connection = sqlite3.connect(db, check_same_thread=False)
        self.cur = self.connection.cursor()
        sql = """
        CREATE TABLE IF NOT EXISTS tasks(
            id Integer Primary Key,
            title text not null,
            description text not null,
            priority integer not null,
            progress text not null,
            member text not null,
            timestamp timestamp not null
        );
        """

        self.cur.execute(sql)
        self.connection.commit()
        self.sync_tasks_from_peers()
        # self.tasks : List[TaskWithId] = self.fetch_all_tasks()

    def sync_tasks_from_peers(self):
        # fetch tasks from the other dbs
        for peer in self.peers:
            tasks = self._get_tasks_from_peer(peer)
            for task in tasks:
                self.update(task, update_peers=False)

    @staticmethod
    def _get_tasks_from_peer(peer: Peer) -> List[Optional[Task]]:
        """
        fetch all tasks from a db
        :param peer: info of a peer db
        :return: list of tasks
        """
        try:
            tasks = [Task(**t) for t in requests.get(
                url="http://{}:{}/tasks_all".format(peer["ip"], peer["port"])).json()]
        except requests.ConnectionError:
            tasks = []
        return tasks

    def _insert(self, task: Task) -> Task:
        """
        insert task to the db
        :return: the inserted task
        """
        #  negative number is the indicator of brand-new task
        if task.task_id < 0:
            self.cur.execute("SELECT id from tasks order by id desc limit 1")
            task_id = self.cur.fetchall()[0][0] + 1
            task.task_id = task_id
        self.cur.execute("insert into tasks values (?,?,?,?,?,?,?)",
                         (task.task_id, *task.unpack()))
        self.connection.commit()
        return task

    def _update(self, task: Task):
        """
        update task in the db
        """
        self.cur.execute(
            "update tasks set title=?, description=?, priority=?, progress=?, member=?, timestamp=? where id=?",
            (*task.unpack(), task.task_id))
        self.connection.commit()

    def fetch_all_tasks(self) -> List[Task]:
        """
        Fetch All tasks from DB
        :return: list of tasks from the db
        """
        self.cur.execute("SELECT * from tasks")
        rows = self.cur.fetchall()
        return [Task(*row) for row in rows]

    def fetch_ongoing_tasks(self) -> List[Task]:
        """
        Fetch All tasks from the db that their progress is not REMOVED
        :return: list of tasks from the db
        """
        self.cur.execute(f"SELECT * from tasks where progress not like '{consts.ProgressVals.REMOVED_VALUE}'")
        rows = self.cur.fetchall()
        return [Task(*row) for row in rows]

    def fetch_task_by_id(self, task_id: int) -> Task:
        """
        Fetch task from DB
        :param task_id: id of the task
        :return: task from the db
        """
        if not self.is_task_exists(task_id):
            raise TaskNotFoundException(f"task id {task_id} was not found in database")
        else:
            self.cur.execute("SELECT * from tasks where id=?", (task_id,))
            rows = self.cur.fetchall()
        return Task(*rows[0])

    #
    def remove(self, task_id: int):
        """
        update a task to removed state
        :param task_id: the id of the task
        :return:
        """
        try:
            task = self.fetch_task_by_id(task_id)
        except TaskNotFoundException:
            raise
        if task.progress == consts.ProgressVals.DONE_VALUE:
            task.timestamp = 0
            task.progress = consts.ProgressVals.REMOVED_VALUE
            self.update(task)
        else:
            raise TaskNotDoneException

    def update(self, task: Task, update_peers: bool = True):
        """
        update the task if needed
        zero timestamp signals to calculate the time from now
        updates only if timestamp is more up-to-date
        updates the peers accordingly
        :param task: the task needed to be updated in the db
        :param update_peers: whether to send update to the peers
        :return: the task
        """
        print(f"update: timestamp in begin: {task.timestamp}, task_id: {task.task_id}")
        task.timestamp = task.timestamp or datetime.datetime.now().timestamp()
        if self.is_task_exists(task.task_id):
            task_in_db = self.fetch_task_by_id(task.task_id)
            print(
                f"task_in_db: {task_in_db.timestamp}, task: {task.timestamp}, {task_in_db.timestamp < task.timestamp}")
            if task_in_db.timestamp < task.timestamp:
                self._update(task)
            else:
                return
        else:
            task = self._insert(task)
        if update_peers:
            print(f"update: {task.task_id}, update_peers")
            self.send_data_to_peers([task])
        return task

    def send_data_to_peers(self, tasks_list: List[Task]):
        print("send_data_to_peers:")
        for peer in self.peers:
            print(f"send_data_to_peers: update {peer['ip']} with {[task.task_id for task in tasks_list]}")
            for task in tasks_list:
                p = multiprocessing.Process(target=self._send_task_to_peer, args=(peer["ip"], peer["port"], task))
                p.start()

    @staticmethod
    def _send_task_to_peer(peer_ip: str, peer_port: int, task: Task):
        """
        sends a task to a peer
        :param peer_ip:
        :param peer_port:
        :param task: a task to send
        :return:
        """
        print(f"_send_task_to_peer: send task: {task.task_id} to {peer_ip}, {peer_port}")
        # header is changed to identify that the request is from another peer,
        # and the peer is not needed to update the other peers
        requests.post(url="http://{peer_ip}:{peer_port}/tasks/{task_id}".format(
            peer_ip=peer_ip, peer_port=peer_port, task_id=task.task_id),
            json=task2dict(task), headers={'host': INTERNAL_REQUEST})

    def is_task_exists(self, task_id: int):
        """
        checks if the task id exist in the db
        :param task_id: the id of the task
        :return: whether the task exists
        """
        self.cur.execute("SELECT count(id) from tasks where id=?", (task_id,))
        ans = self.cur.fetchall()[0][0]
        return ans > 0
