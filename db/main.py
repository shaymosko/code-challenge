import uvicorn
import argparse
from utils import get_server_by_name


if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser(description="an instance the server side of the todo list")
    arg_parser.add_argument("--name", dest="name", required=True, help="the name of the server (affects hostname)")
    args = arg_parser.parse_args()

    server_info = get_server_by_name(args.name)
    server_ip = server_info["ip"]
    server_port = server_info["port"]
    print(server_ip, server_port)
    uvicorn.run("server:app", port=server_port, host=server_ip,
                reload=False, access_log=True, log_level=uvicorn.config.TRACE_LOG_LEVEL)
