import fastapi

from tasks import Task, TaskDict, TaskNotFoundException, TaskDictWithoutId, TaskNotDoneException
from fastapi import FastAPI, status, HTTPException
# from dbsync import DBSync
from database import Database
from utils import get_known_servers_bindings
from consts import INTERNAL_REQUEST

# db = DBSync(peers=get_known_servers_bindings())
app = FastAPI(debug=True)

servers = get_known_servers_bindings()
db = Database()


@app.get("/tasks_all")
def get_all_tasks():
    """
    get all tasks from the db
    """
    return db.fetch_all_tasks()


@app.get("/tasks")
def root():
    """
    returns all tasks that are not removed
    :return:
    """
    return db.fetch_ongoing_tasks()


@app.post("/tasks/{task_id}")
def update_item(task_id: int, task: TaskDictWithoutId, request: fastapi.Request):
    """

    :param task_id: id of the task
    :param task: information of the task to be updated
    :param request: Request object
    :return:
    """
    # task.update({'task_id': task_id})
    task = Task(task_id=task_id, **task)
    print(f"update_item: {request.headers['host']}")
    # if INTERNAL_REQUEST in the headers, there is no need to update the peers
    db.update(task, update_peers=request.headers['host'] != INTERNAL_REQUEST)


@app.post("/tasks/{task_id}/remove")
def remove_task(task_id: int):
    """
    removes a task from the board
    :param task_id: id of the task
    :return:
    """
    try:
        db.remove(task_id)
    except TaskNotFoundException:
        raise HTTPException(status.HTTP_404_NOT_FOUND, f"task {task_id} not found")
    except TaskNotDoneException:
        raise HTTPException(status.HTTP_403_FORBIDDEN, f"task {task_id} is not done")


@app.put('/tasks/new')
def create_new_task(task: TaskDictWithoutId):
    """
    creates new task, must have an id of negative number
    :param task:
    :return:
    """
    db.update(Task({'task_id': -1, **task}))
