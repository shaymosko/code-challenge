from typing import TypedDict, List
import json

from consts import SERVERS_LIST_JSON


class Peer(TypedDict):
    ip: str
    port: int


def get_server_by_name(server_name: str) -> Peer:
    """
    read servers.json and returns the servers ip and port of the server
    :param server_name: requested server name
    :return : Peer object if found
    """
    with open(SERVERS_LIST_JSON, "r") as f:
        data = json.load(f)
    ip, port = data[server_name]
    return Peer(ip=ip, port=port)


def get_known_servers_bindings() -> List[Peer]:
    """
    read servers.json and returns the list of known servers
    :return: list of server bindings
    """
    with open(SERVERS_LIST_JSON, "r") as f:
        data = json.load(f)
    return list(map(lambda name: Peer(ip=data[name][0], port=data[name][1]), data.keys()))
