from typing import Tuple, TypedDict


class TaskNotFoundException(Exception):
    pass


class TaskNotDoneException(Exception):
    pass


class Task:
    def __init__(self, task_id: int, title: str, description: str, priority: int, progress: str, member: str,
                 timestamp: float) -> None:
        self.task_id = task_id
        self.title = title
        self.description = description
        self.priority = priority
        self.progress = progress
        self.member = member
        self.timestamp = timestamp

    def unpack(self) -> Tuple[str, str, int, str, str, float]:
        # returns the tasks' info without the id
        return self.title, self.description, self.priority, self.progress, self.member, self.timestamp


class TaskDictWithoutId(TypedDict):
    title: str
    description: str
    priority: int
    progress: str
    member: str
    timestamp: float


class TaskDict(TaskDictWithoutId):
    id: int


def task2dict(task: Task) -> TaskDict:
    return TaskDict(task.__dict__)

# def dict2task(task: TaskDict) -> Task:
#     return Task(*map(lambda x: x[1], task.items()))
